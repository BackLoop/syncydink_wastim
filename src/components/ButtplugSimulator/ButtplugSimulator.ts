import Vue from "vue";
import { Component, Model, Prop, Watch } from "vue-property-decorator";
import { ButtplugMessage, ButtplugDeviceMessage, FleshlightLaunchFW12Cmd } from "buttplug";
import * as TWEEN from "@tweenjs/tween.js";
import FleshlightLaunchSimulatorComponent from "./FleshlightLaunchSimulator/FleshlightLaunchSimulator.vue";
import WastimComponent from "./WAstimSimulator/WAstimSimulator.vue";
import VibratorSimulatorComponent from "./VibratorSimulator/VibratorSimulator.vue";

const fleshlightSmallIcon = require("../../../static/images/fleshlight-small.png");
const wastimSmallIcon = require("../../../static/images/wastim-250.png");
const hushSmallIcon = require("../../../static/images/hush-250.png");
const noraSmallIcon = require("../../../static/images/nora-250.png");
const errorIcon = require("../../../static/images/error.png");

@Component({
  components: {
    FleshlightLaunchSimulatorComponent,
    WastimComponent,
    VibratorSimulatorComponent,
  },
})
export default class ButtplugSimulator extends Vue {

  @Prop()
  private currentMessages: ButtplugMessage[];
  @Prop()
  private paused: boolean;
  private vibratorImageURL: string = "";
  private modes: any = [
    {
      name: "Fleshlight Launch",
      imageurl: fleshlightSmallIcon,
    },
    {
      name: "WebAudio Stim",
      imageurl: wastimSmallIcon,
    },
    {
      name: "Lovense Hush",
      imageurl: hushSmallIcon,
    },
    {
      name: "Lovense Nora",
      imageurl: noraSmallIcon,
    },
  ];
  private fleshlightMode: boolean = true;
  private wastimMode: boolean = false;
  private vibratorMode: boolean = false;
  private onModeChange(mode: any) {
    if (mode.name === "Fleshlight Launch") {
      this.fleshlightMode = true;
      this.wastimMode = false;
      this.vibratorMode = false;
    } else if (mode.name === "WebAudio Stim") {
      this.fleshlightMode = false;
      this.wastimMode = true;
      this.vibratorMode = false;
      this.vibratorImageURL = mode.imageurl;
    } else if ((mode.name === "Lovense Hush") || (mode.name === "Lovense Nora")) {
      this.fleshlightMode = false;
      this.wastimMode = false;
      this.vibratorMode = true;
      this.vibratorImageURL = mode.imageurl;
    } else {
      this.fleshlightMode = false;
      this.wastimMode = false;
      this.vibratorMode = false;
      this.vibratorImageURL = errorIcon;
    }
  }
}
