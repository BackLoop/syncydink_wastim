import Vue from "vue";
import { Component, Model, Prop, Watch } from "vue-property-decorator";
import { ButtplugMessage, ButtplugDeviceMessage, FleshlightLaunchFW12Cmd, SingleMotorVibrateCmd } from "buttplug";
import * as TWEEN from "@tweenjs/tween.js";
import io from "socket.io-client";
const wastimBalanceIcon = require("../../../../static/images/balance.png");
const wastimVolumeIcon = require("../../../../static/images/volume.png");

@Component({})
export default class WAstimSimulator extends Vue {
  private wasocket = io("http://localhost:6969");
  private wastim: HTMLElement;
  // private currentPosition: any = { x: 0, y: 0 };
  // private lastPosition: number = 0;
  private wastimStyle: any = {
    bottom: "0%",
  };
  // private pauseTime: number = -1;
  @Prop()
  private currentMessages: ButtplugMessage[];
  @Prop()
  private paused: boolean;
  private slider: string = "sl32";
  private modes: any = [
    {
      name: "Balance",
      imageurl: wastimBalanceIcon,
      slider: "sl32",
    },
    {
      name: "Master Volume",
      imageurl: wastimVolumeIcon,
      slider: "sl33",
    },
  ];

  public mounted() {
    this.wastim = document.getElementById("wastim-image")!;
  }

  @Watch("paused")
  private onPauseChange() {
    if (!this.paused) {
      // requestAnimationFrame(this.animate);
    }
  }

  @Watch("currentMessages")
  private move() {
    if (this.currentMessages.length === 0) {
      return;
    }
    for (const msg of this.currentMessages!) {
      if (msg.getType() !== "SingleMotorVibrateCmd") {
         continue;
      }
      const waMsg: SingleMotorVibrateCmd = msg as SingleMotorVibrateCmd;
      // const p = -((100 - (waMsg.Speed * 100) ) * 0.22);
      // const duration = 1;
      // new TWEEN.Tween(this.currentPosition)
      //   .to({x: 0, y: p}, duration)
      //   .start();
      // requestAnimationFrame(this.animate);
      this.wasocket.emit("slider", { name: this.slider, value: (waMsg.Speed * 100) });
    }
  }

  private onModeChange(mode: any) {
      this.slider = mode.slider;
    }
  }

/*   private animate(timestamp: number) {
    if (!TWEEN.update() || this.paused) {
      return;
    }
    this.wastimStyle.bottom = `${this.currentPosition.y}%`;
    requestAnimationFrame(this.animate);
  }
 */
  // positions returns the current position in percent (0-100).
 /*  private position() {
    const bottomPx = parseFloat(this.wastim!.style.bottom!);
    const widgetHeightPx = document.getElementById("buttplug-simulator-component")!.clientHeight;
    const percentValue = bottomPx / widgetHeightPx * 100;
    return Math.round(percentValue / .22);
  }
 */
  // moveDuration returns the time in milliseconds it will take to move
  // to position at speed.
  //
  // position: position in percent (0-100).
  // speed:    speed in percent (20-100).
 /*  private moveDuration(position: number, speed: number) {
    const distance = Math.abs(position - this.lastPosition);
    this.lastPosition = position;
    return this.calcDuration(distance, speed);
  } */

  // _calcDuration returns duration of a move in milliseconds for a given
  // distance/speed.
  //
  // distance: amount to move percent (0-100).
  // speed: speed to move at in percent (20-100).
 /*  private calcDuration(distance: number, speed: number) {
    return Math.pow(speed / 25000, -0.95) / (90 / distance);
  }
} */

// Some code in this file taken from https://github.com/funjack/launchcontrol
// MIT License:
/*
Lauchcontrol UI Fleshlight

https://github.com/funjack/launchcontrol

Copyright 2017 Funjack

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
